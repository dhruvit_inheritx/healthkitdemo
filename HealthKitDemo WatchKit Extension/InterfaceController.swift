//
//  InterfaceController.swift
//  HealthKitDemo WatchKit Extension
//
//  Created by Dhruvit on 31/03/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity
import CoreLocation
import CoreMotion
import HealthKit
import UserNotifications
import UIKit

class InterfaceController: WKInterfaceController {
    
    var watchSession : WCSession?
    
    // Outlets
    @IBOutlet var lblState : WKInterfaceLabel!
    @IBOutlet var lblStep : WKInterfaceLabel!
    @IBOutlet var lblDistance : WKInterfaceLabel!
    @IBOutlet var lblFloorsState : WKInterfaceLabel!
    
    // Variables
    
    var pedometer = CMPedometer()
    var accelerometer = CMMotionManager()
    var healthStore = HKHealthStore()
    var days:[String] = []
    var stepsTaken:[Int] = []
    
    let activityManager = CMMotionActivityManager()
    var strState = ""
    var strStep = ""
    var strDistance = ""
    var strFloorsState = ""
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
        self.sendDataToApp()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        pedometer.stopUpdates()
    }
    
    @IBAction func sendData() {
        
        let arrayData = [strState,strStep,strDistance,strFloorsState]
        
        watchSession!.sendMessage ([ "data"  : arrayData ] as [String:Any],
                                   replyHandler :  {  ( response )  in
                                    print(response)
                                    
        },  errorHandler :  {  ( error )  in
            
            print( "Error sending message:% @ " ,  error )
        });
    }
    
    func sendDataToApp() {
        
        if #available(watchOSApplicationExtension 3.0, *) {
            pedometer.startEventUpdates { (event, error) in
                
            }
        } else {
            // Fallback on earlier versions
        }
        
        if (WCSession.isSupported()) {
            watchSession = WCSession.default()
            watchSession?.delegate = self
            watchSession?.activate()
        }
        
        if (CMMotionActivityManager.isActivityAvailable()) {
            self.activityManager.startActivityUpdates(to: OperationQueue.main, withHandler: { (activity) in
                
                DispatchQueue.main.async(execute: {
                    
                    if activity?.stationary == true {
                        print("Sitting")
                        self.lblStep.setText("Sitting")
                        self.strState = "Sitting"
                    }
                    else if activity?.walking == true {
                        print("Walking")
                        self.lblStep.setText("Walking")
                        self.strState = "Walking"
                    }
                    else if activity?.running == true {
                        print("Running")
                        self.lblStep.setText("Running")
                        self.strState = "Running"
                        
                    }
                    else if activity?.automotive == true {
                        print("Automotive")
                        self.lblStep.setText("Automotive")
                        self.strState = "Running"
                    }
                    
                })
            })
        }
        
        var calander = NSCalendar.current
        var components = calander.dateComponents([.year,.month,.day,.hour,.minute,.second], from: Date())
        components.hour = 0
        components.minute = 0
        components.second = 0
        
        let timeZone = NSTimeZone.system
        calander.timeZone = timeZone
        let midNightOfDay = calander.date(from: components)
        
        if CMPedometer.isStepCountingAvailable() {
            
            let fromDate = Date(timeIntervalSinceNow: -86400 * 7)
            self.pedometer.queryPedometerData(from: fromDate, to: Date(), withHandler: { (data, error) in
                
                print("Pedometer Data :- \(data)")
                
                DispatchQueue.main.async(execute: {
                    
                    if error == nil {
                        print(data?.numberOfSteps ?? 0)
                        
                        self.strStep = String(format: "%d", (data?.numberOfSteps.intValue)!)
                        self.lblStep.setText(self.strStep)
                        
                        self.strDistance = String(format: "%d", (data?.distance?.floatValue)!)
                        self.lblDistance.setText(self.strDistance)
                        
                        if let floorsAscended = data?.floorsAscended
                        {
                            self.strFloorsState = String(format: "floorsAscended:  %lu", floorsAscended.uintValue)
                            self.lblFloorsState.setText(self.strFloorsState)
                        }
                        
                        if let floorsDescended = data?.floorsDescended
                        {
                            self.strFloorsState = String(format: "floorsDescended: %lu", floorsDescended.uintValue)
                            self.lblFloorsState.setText(self.strFloorsState)
                        }
                        
                        if let floorsDescended = data?.currentPace {
                            self.strFloorsState = String(format: "currentPace: %lu", floorsDescended.uintValue)
                            self.lblFloorsState.setText(self.strFloorsState)
                        }
                        
                        if let floorsDescended = data?.currentCadence {
                            self.strFloorsState = String(format: "currentPace: %lu", floorsDescended.uintValue)
                            self.lblFloorsState.setText(self.strFloorsState)
                        }
                        
                        self.sendData()
                    }
                })
            })
            
            self.pedometer.startUpdates(from: midNightOfDay!, withHandler: { (data, error) in
                
                DispatchQueue.main.async(execute: { 
                    if error == nil {
                        print("numberOfSteps :- \(data?.numberOfSteps)")
                    }
                })
            })
        }
    }
}

extension InterfaceController : WCSessionDelegate {
    
    @available(watchOSApplicationExtension 2.2, *)
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("session active")
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("session received message :- \(message)")
    }
    
}
